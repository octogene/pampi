# -*- coding: utf-8 -*-

# -----------------------------------------------------------------
# This file is a part of PAMPI project.
# Name:         PAMPI
# Copyright:    (C) 2017 Pascal PETER
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
# -----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------

"""
DESCRIPTION :
    contient l'interface graphique.
    La plupart des actions renvoient à des fonctions qui sont dans les
    modules utils_aaa.
"""

# importation des modules utiles :
from __future__ import division, print_function

# importation des modules perso :
import utils
import utils_functions
import utils_filesdirs
import utils_editor

# PyQt5, PyQt4 ou PySide :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
    from PyQt5 import QtWebKitWidgets
elif utils.PYQT == 'PYQT4':
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui
    from PyQt4 import QtWebKit as QtWebKitWidgets
else:
    from PySide import QtCore, QtGui as QtWidgets, QtGui
    from PySide import QtWebKit as QtWebKitWidgets



class MainWindow(QtWidgets.QMainWindow):
    """
    LA FENÊTRE PRINCIPALE
    """
    def __init__(self, locale, translator, parent=None):
        """
        mise en place de l'interface
        plus des variables utiles
        """
        super(MainWindow, self).__init__(parent)
        # i18n :
        self.translator = translator
        self.locale = locale
        # le dossier du logiciel et des fichiers :
        self.beginDir = QtCore.QDir.currentPath()
        self.configDir, first = utils_filesdirs.createConfigAppDir(utils.PROGNAME)
        self.tempPath = utils_filesdirs.createTempAppDir(utils.PROGNAME)
        self.pandocBin = self.searchPandoc()

        # on lit le fichier de config :
        self.configDict = utils_filesdirs.readConfigFile(self.configDir)

        # on recherche le dossier des présentations :
        self.presentationsDir = self.configDict['OTHER']['presentationsDir']
        if not(QtCore.QFileInfo(self.presentationsDir).isDir()):
            if utils.PYQT == 'PYQT5':
                documentsLocation = QtCore.QStandardPaths.standardLocations(
                    QtCore.QStandardPaths.DocumentsLocation)
            else:
                documentsLocation = QtGui.QDesktopServices.storageLocation(
                    QtGui.QDesktopServices.DocumentsLocation)
            if isinstance(documentsLocation, list):
                documentsLocation = documentsLocation[0]
            #print(documentsLocation, type(documentsLocation))
            if not(QtCore.QFileInfo(documentsLocation).isDir()):
                # sélection du dossier de destination :
                documentsLocation = QtWidgets.QFileDialog.getExistingDirectory(
                    self, 
                    QtWidgets.QApplication.translate(
                        'main', 'Select a directory for presentations'), 
                    documentsLocation, 
                    QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
                if isinstance(documentsLocation, tuple):
                    documentsLocation = documentsLocation[0]
            #print('documentsLocation:', documentsLocation)
            # on recopie le dossier des présentations livré avec le logiciel :
            utils_functions.doWaitCursor()
            utils_filesdirs.createDirs(
                documentsLocation, 'presentations')
            utils_filesdirs.copyDir(
                utils_functions.u('{0}/presentations').format(
                    self.beginDir), 
                utils_functions.u('{0}/presentations').format(
                    documentsLocation))
            #print('DOSSIER COPIE')
            utils_functions.restoreCursor()
            self.presentationsDir = utils_functions.u(
                '{0}/presentations').format(documentsLocation)
        #print('presentationsDir:', self.presentationsDir)

        # on vérifie l'existence des derniers fichiers :
        lastFiles = []
        for fileName in self.configDict['LASTFILES']:
            longFileName = utils_functions.u('{0}/md/{1}').format(
                self.presentationsDir, fileName)
            if QtCore.QFileInfo(longFileName).isFile():
                lastFiles.append(fileName)
        self.configDict['LASTFILES'] = lastFiles

        # le nom du fichier :
        self.actualFile = {
            'baseName': '', 
            'mustSave': False}

        # mise en place de l'interface :
        utils.loadSupportedImageFormats()
        self.createInterface()
        self.updateWindowTitle()
        self.updateLastFilesMenu()
        self.markdownEditor.textChanged.connect(self.textChanged)
        rect = QtWidgets.QApplication.desktop().availableGeometry()
        self.resize(3 * rect.width() / 4, 3 * rect.height() / 4)
        self.showHelp(here=True)
        self.markdownEditor.setFocus()

    def createInterface(self):
        # barre d'outils, actions et menu :
        self.toolBar = QtWidgets.QToolBar(self)
        self.toolBar.setIconSize(
            QtCore.QSize(
                utils.STYLE['PM_LargeIconSize'], 
                utils.STYLE['PM_LargeIconSize']))
        self.toolBar.setToolButtonStyle(QtCore.Qt.ToolButtonIconOnly)
        self.addToolBar(QtCore.Qt.LeftToolBarArea, self.toolBar)

        # ZONE DE GAUCHE
        # titre de la présentation :
        text = QtWidgets.QApplication.translate(
            'main', 'Title:')
        titleLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.titleEdit = QtWidgets.QLineEdit()
        text = QtWidgets.QApplication.translate(
            'main', 
            'Here you can define the title '
            'that will be displayed in the browser')
        titleLabel.setToolTip(text)
        self.titleEdit.setToolTip(text)
        # fichier CSS de la présentation :
        text = QtWidgets.QApplication.translate(
            'main', 'CSS:')
        cssLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.cssEdit = QtWidgets.QLineEdit()
        text = QtWidgets.QApplication.translate(
            'main', 
            'If you are using a custom CSS file, '
            'enter its name here (without extension)')
        cssLabel.setToolTip(text)
        self.cssEdit.setToolTip(text)
        # agencement :
        leftLayout = QtWidgets.QVBoxLayout()
        leftLayout.addWidget(titleLabel)
        leftLayout.addWidget(cssLabel)
        rightLayout = QtWidgets.QVBoxLayout()
        rightLayout.addWidget(self.titleEdit)
        rightLayout.addWidget(self.cssEdit)
        configLayout = QtWidgets.QHBoxLayout()
        configLayout.addLayout(leftLayout)
        configLayout.addLayout(rightLayout)
        # un QTextEdit pour afficher le fichier Markdown :
        self.markdownEditor = utils_editor.markdownEditor(self)
        # le tout dans la zone de gauche :
        leftLayout = QtWidgets.QVBoxLayout()
        leftLayout.addLayout(configLayout)
        leftLayout.addWidget(self.markdownEditor)
        leftGroup = QtWidgets.QGroupBox('')
        leftGroup.setLayout(leftLayout)

        # ZONE DE DROITE
        # le bouton d'affichage des exemples :
        self.showExamplesButton = QtWidgets.QToolButton()
        self.showExamplesButton.setToolButtonStyle(
            QtCore.Qt.ToolButtonTextBesideIcon)
        # un QCheckBox pour les rendre éditables :
        self.editExamplesCheckBox = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate('main', 'Edit Cheat Sheet'))
        text = QtWidgets.QApplication.translate(
            'main', 'Make the file editable to add your own examples')
        self.editExamplesCheckBox.setToolTip(
            QtWidgets.QApplication.translate(
                'main', 
                'Make the file editable to add your own examples'))
        self.editExamplesCheckBox.setChecked(False)
        self.editExamplesCheckBox.setVisible(False)
        self.editExamplesCheckBox.stateChanged.connect(self.editExamples)
        # on les agence dans un QHBoxLayout :
        buttonsLayout = QtWidgets.QHBoxLayout()
        buttonsLayout.addWidget(self.showExamplesButton)
        buttonsLayout.addWidget(self.editExamplesCheckBox)
        buttonsLayout.addStretch()
        # un QWebView pour afficher le fichier html :
        self.helpWebView = QtWebKitWidgets.QWebView(self)
        self.helpWebView.page().setLinkDelegationPolicy(
            QtWebKitWidgets.QWebPage.DelegateAllLinks)
        self.helpWebView.linkClicked.connect(self.linkClicked)
        container = QtWidgets.QAbstractScrollArea()
        vBoxLayout = QtWidgets.QVBoxLayout()
        vBoxLayout.addWidget(self.helpWebView)
        container.setLayout(vBoxLayout)
        # un QTextEdit pour afficher les exemples :
        self.samplesEditor = utils_editor.markdownEditor(
            self, readOnly=True)
        self.examplesFile = utils_functions.doLocale(
            self.locale, 
            utils_functions.u(
                '{0}/assets/EXAMPLES').format(self.presentationsDir), 
            '.md')
        # lecture du fichier :
        lines = utils_filesdirs.readTextFile(self.examplesFile)        
        self.samplesEditor.setPlainText(lines)

        # le whatStackedWidget pour passer de l'un à l'autre :
        self.whatStackedWidget = QtWidgets.QStackedWidget()
        self.whatStackedWidget.addWidget(container)
        self.whatStackedWidget.addWidget(self.samplesEditor)
        # le tout dans la zone de droite :
        rightLayout = QtWidgets.QVBoxLayout()
        rightLayout.addLayout(buttonsLayout)
        rightLayout.addWidget(self.whatStackedWidget)
        rightGroup = QtWidgets.QGroupBox('')
        rightGroup.setLayout(rightLayout)

        # mise en place :
        hsplitter = QtWidgets.QSplitter(self)
        hsplitter.setOrientation(QtCore.Qt.Horizontal)
        hsplitter.addWidget(leftGroup)
        hsplitter.addWidget(rightGroup)
        rect = QtWidgets.QApplication.desktop().availableGeometry()
        hsplitter.setSizes([rect.width() / 3, rect.width() / 2])
        self.stackedWidget = QtWidgets.QStackedWidget()
        self.stackedWidget.addWidget(hsplitter)
        self.setCentralWidget(self.stackedWidget)
        self.closing = False
        # le reste de l'interface :
        self.createActions()
        self.createMenusAndButtons()

    def createActions(self):
        self.actionQuit = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Exit'), 
            self, 
            icon=utils.doIcon('application-exit'), 
            shortcut=QtWidgets.QApplication.translate('main', 'Ctrl+Q'))
        self.actionQuit.triggered.connect(self.close)

        self.actionOpen = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Open'), 
            self, 
            icon=utils.doIcon('document-open'), 
            shortcut=QtWidgets.QApplication.translate('main', 'Ctrl+O'))
        self.actionOpen.triggered.connect(self.openFile)

        self.actionSave = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Save'), 
            self, 
            icon=utils.doIcon('document-save'), 
            shortcut=QtWidgets.QApplication.translate('main', 'Ctrl+S'))
        self.actionSave.triggered.connect(self.saveFile)

        self.actionSaveAs = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Save as...'), 
            self, 
            icon=utils.doIcon('document-save-as'), 
            shortcut=QtWidgets.QApplication.translate('main', 'Ctrl+Shift+S'))
        self.actionSaveAs.triggered.connect(self.saveFileAs)

        self.actionExport = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Export the presentation'), 
            self, 
            icon=utils.doIcon('presentation-export'))
        self.actionExport.triggered.connect(self.export)

        self.actionSaveDoShow = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Save, convert and preview'), 
            self, 
            icon=utils.doIcon('icon'))
        self.actionSaveDoShow.triggered.connect(self.saveDoShow)

        self.actionDoPandoc = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Convert to html with Pandoc'), 
            self, 
            icon=utils.doIcon('run-build'))
        self.actionDoPandoc.triggered.connect(self.doPandoc)

        self.actionPreview = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Preview presentation in PAMPI'), 
            self, 
            icon=utils.doIcon('layer-visible-on'))
        self.actionPreview.triggered.connect(self.preview)

        self.actionLaunchInBrowser = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Open in the browser'), 
            self, 
            icon=utils.doIcon('internet-web-browser'))
        self.actionLaunchInBrowser.triggered.connect(self.launchInBrowser)

        self.actionWithMathJax = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Use MathJax'), 
            self, 
            icon=utils.doIcon('MathJax'))
        self.actionWithMathJax.setCheckable(True)

        self.actionOpenPresentationDir = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Open the presentation folder'), 
            self, 
            icon=utils.doIcon('folder-presentations'))
        self.actionOpenPresentationDir.triggered.connect(self.openPresentationDir)

        self.actionChangePresentationDir = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Change the presentation folder'), 
            self, 
            icon=utils.doIcon('folder-presentations-configure'))
        self.actionChangePresentationDir.triggered.connect(self.changePresentationDir)

        self.actionCreateDesktopFileLinux = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Create a launcher'), 
            self, 
            icon=utils.doIcon('logo_linux'))
        self.actionCreateDesktopFileLinux.triggered.connect(self.doCreateDesktopFileLinux)

        self.actionHelp = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Help'), 
            self, 
            icon=utils.doIcon('help'))
        self.actionHelp.triggered.connect(self.showHelp)

        self.actionAbout = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'About'), 
            self, 
            icon=utils.doIcon('help-about'))
        self.actionAbout.triggered.connect(self.about)

        self.actionShowExamples = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Cheat Sheet'), 
            self, 
            icon=utils.doIcon('help-hint'))
        self.actionShowExamples.setCheckable(True)
        self.actionShowExamples.triggered.connect(self.showExamples)
        self.actionShowExamples.setToolTip(
            QtWidgets.QApplication.translate(
                'main', 
                'A list of examples to copy-paste'))
        self.showExamplesButton.setDefaultAction(self.actionShowExamples)

        self.actionWizard = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Wizard'), 
            self, 
            icon=utils.doIcon('tools-wizard'))
        self.actionWizard.triggered.connect(self.wizard)

    def createMenusAndButtons(self):
        """
        blablabla
        """
        self.fileMenu = QtWidgets.QMenu(
            QtWidgets.QApplication.translate('main', 'File'), self)
        self.fileMenu.addAction(self.actionOpen)
        self.lastFilesMenu = QtWidgets.QMenu(
            QtWidgets.QApplication.translate('main', 'Recent Files'), self)
        self.lastFilesMenu.setIcon(
            utils.doIcon('document-open-recent'))
        self.fileMenu.addMenu(self.lastFilesMenu)
        self.fileMenu.addAction(self.actionWizard)
        self.fileMenu.addAction(self.actionSave)
        self.fileMenu.addAction(self.actionSaveAs)
        self.fileMenu.addAction(self.actionExport)
        if utils.OS_NAME[0] == 'linux':
            self.fileMenu.addSeparator()
            self.fileMenu.addAction(self.actionCreateDesktopFileLinux)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.actionQuit)

        self.toolsMenu = QtWidgets.QMenu(
            QtWidgets.QApplication.translate('main', 'Tools'), self)
        self.toolsMenu.addAction(self.actionSaveDoShow)
        self.toolsMenu.addSeparator()
        #self.toolsMenu.addAction(self.actionDoPandoc)
        #self.toolsMenu.addAction(self.actionPreview)
        self.toolsMenu.addAction(self.actionLaunchInBrowser)
        self.toolsMenu.addAction(self.actionWithMathJax)
        self.toolsMenu.addSeparator()
        self.toolsMenu.addAction(self.actionOpenPresentationDir)
        self.toolsMenu.addAction(self.actionChangePresentationDir)

        self.helpMenu = QtWidgets.QMenu(
            QtWidgets.QApplication.translate('main', 'Help'), self)
        self.helpMenu.addAction(self.actionHelp)
        self.helpMenu.addAction(self.actionAbout)
        self.helpMenu.addSeparator()
        self.helpMenu.addAction(self.actionShowExamples)

        self.menuBar().addMenu(self.fileMenu)
        self.menuBar().addMenu(self.toolsMenu)
        self.menuBar().addMenu(self.helpMenu)

        # la toolBar :
        self.toolBar.addAction(self.actionQuit)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionOpen)
        self.toolBar.addAction(self.actionSave)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionSaveDoShow)
        self.toolBar.addAction(self.actionWithMathJax)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionLaunchInBrowser)
        self.toolBar.addAction(self.actionOpenPresentationDir)

    def textChanged(self):
        """
        pour prendre en compte dans le titre 
        le fait que le texte a été modifié
        """
        if self.markdownEditor.noChange:
            self.markdownEditor.noChange = False
            return
        self.updateWindowTitle(mustSave=True)

    def updateLastFilesMenu(self, baseName=''):
        """
        mise à jour du menu des fichiers récents
        """
        if len(baseName) > 0:
            if (baseName in self.configDict['LASTFILES']):
                self.configDict['LASTFILES'].remove(baseName)
            self.configDict['LASTFILES'].insert(0, baseName)
        self.lastFilesMenu.clear()
        for fileName in self.configDict['LASTFILES']:
            newAction = QtWidgets.QAction(fileName, self)
            newAction.triggered.connect(self.openFile)
            newAction.setData(fileName)
            self.lastFilesMenu.addAction(newAction)

    def updateWindowTitle(self, mustSave=False):
        """
        mise à jour du titre de la fenêtre.
        Suivant que le fichier est enregistré, a été modifié, etc
        """
        self.actualFile['mustSave'] = mustSave
        if mustSave:
            mustSaveText = '*'
        else:
            mustSaveText = ''
        if len(self.actualFile['baseName']) < 1:
            windowTitle = utils_functions.u('{0}  []').format(
                utils.PROGTITLE)
        else:
            windowTitle = utils_functions.u('{0} {1}[{2}]').format(
                utils.PROGTITLE, mustSaveText, self.actualFile['baseName'])
        self.setWindowTitle(windowTitle)
        fileNamed = ((self.actualFile['baseName'] != '') and (self.pandocBin != ''))
        for action in (
            self.actionExport, 
            self.actionSaveDoShow, 
            self.actionDoPandoc, 
            self.actionPreview, 
            self.actionLaunchInBrowser):
            action.setEnabled(fileNamed)

    def testMustSave(self):
        """
        pour savoir s'il faut proposer d'enregistrer le fichier.
        """
        result = True
        if self.actualFile['mustSave']:
            message = QtWidgets.QApplication.translate(
                'main',
                'The file has been modified.\n'
                'Do you want to save your changes?')
            reponseMustSave = utils_functions.messageBox(
                self, level='warning', message=message,
                buttons=['Save', 'Discard', 'Cancel'])
            if reponseMustSave == QtWidgets.QMessageBox.Cancel:
                result = False
            elif reponseMustSave == QtWidgets.QMessageBox.Save:
                self.saveFile()
        return result

    def closeEvent(self, event):
        """
        on quitte.
        suppression du dossier temporaire
        """
        self.closing = True
        if not(self.testMustSave()):
            event.ignore()
            return
        # on enregistre le fichier d'exemple s'il est éditable :
        if self.editExamplesCheckBox.isChecked():
            lines = self.samplesEditor.toPlainText()
            QtCore.QFile(self.examplesFile).remove()
            outFile = QtCore.QFile(self.examplesFile)
            if outFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
                stream = QtCore.QTextStream(outFile)
                stream.setCodec('UTF-8')
                stream << lines
                outFile.close()
        # on écrit le fichier de config :
        utils_filesdirs.writeConfigFile(
            self.configDir, 
            self.configDict, 
            self.presentationsDir)
        # suppression du dossier temporaire :
        utils_filesdirs.emptyDir(
            QtCore.QDir.tempPath() + '/' + utils.PROGNAME)
        QtCore.QDir.temp().rmdir(utils.PROGNAME)
        event.accept()

    def disableInterface(self, dialog):
        """
        cache les barres d'outils lors de l'appel d'un dialog
        affiché dans le stackedWidget.
        On retourne aussi :
            lastIndex : le widget affiché précédemment dans le stackedWidget
            lastTitle : le titre de la fenêtre
        """
        lastIndex = self.stackedWidget.currentIndex()
        lastTitle = self.windowTitle()
        self.menuBar().setVisible(False)
        self.toolBar.setVisible(False)
        self.setWindowTitle(dialog.windowTitle())
        newIndex = self.stackedWidget.addWidget(dialog)
        self.stackedWidget.setCurrentIndex(newIndex)
        return (lastIndex, lastTitle)

    def enableInterface(self, dialog, lastState=(0, '')):
        """
        replace les barres d'outils après l'appel d'un dialog
        affiché dans le stackedWidget.
        On remet aussi l'état précédent via lastState :
            widget affiché dans le stackedWidget
            titre de la fenêtre
        """
        if self.closing:
            return
        self.stackedWidget.removeWidget(dialog)
        self.stackedWidget.setCurrentIndex(lastState[0])
        self.setWindowTitle(lastState[1])
        self.menuBar().setVisible(True)
        self.toolBar.setVisible(True)

    def openFile(self):
        """
        ouverture d'un fichier.
        """

        def readHeader(lines):
            """
            lecture de l'entête perso du fichier.
            Permet de sauvegarder divers réglages 
            dans le fichier md (title, etc)
            """
            title = ''
            css = ''
            withMathJax = False
            try:
                header = lines.split('-->')[0].split('<!--')[1].split('\n')
                for line in header:
                    uline = utils_functions.u(line)
                    if ('TITLE:' in uline):
                        title = uline[6:]
                    elif ('CSS:' in uline):
                        css = uline[4:]
                    elif ('MATHJAX' in uline):
                        withMathJax = True
                lines = '-->'.join(lines.split('-->')[1:])
                lines = '\n'.join(lines.split('\n')[3:])
            except:
                pass
            finally:
                return (title, css, withMathJax, lines)

        if not(self.testMustSave()):
            return
        if self.sender() != self.actionOpen:
            # on a demandé un fichier récent :
            who = 'recent'
            fileName = utils_functions.u('{0}/md/{1}').format(
                self.presentationsDir, self.sender().data())
        else:
            # on commence par sélectionner le fichier :
            who = 'action'
            fileName = QtWidgets.QFileDialog.getOpenFileName(
                self,
                QtWidgets.QApplication.translate('main', 'Open'),
                utils_functions.u('{0}/md').format(self.presentationsDir),
                QtWidgets.QApplication.translate(
                    'main', 'Markdown Files (*.md)'))
            if len(fileName) < 1:
                return
            if isinstance(fileName, tuple):
                fileName = fileName[0]
            # on recopie le fichier dans presentationsDir/md si besoin :
            fileInfo = QtCore.QFileInfo(fileName)
            mdDir = utils_functions.u('{0}/md').format(self.presentationsDir)
            mdDir = QtCore.QDir(mdDir).absolutePath()
            fileDir = QtCore.QDir(fileInfo.canonicalPath()).absolutePath()
            if fileDir != mdDir:
                goodFile = utils_functions.u('{0}/md/{1}.md').format(
                    self.presentationsDir, fileInfo.baseName())
                utils_filesdirs.removeAndCopy(fileName, goodFile)
                fileName = goodFile
        fileInfo = QtCore.QFileInfo(fileName)
        # lecture du fichier :
        lines = utils_filesdirs.readTextFile(fileName)        
        (title, css, withMathJax, lines) = readHeader(lines)
        self.titleEdit.setText(title)
        self.cssEdit.setText(css)
        self.actionWithMathJax.setChecked(withMathJax)
        self.markdownEditor.setPlainText(lines)
        self.actualFile = {
            'baseName': fileInfo.baseName(), 
            'mustSave': False}
        self.updateWindowTitle()
        # on met à jour les fichiers récents :
        baseName = utils_functions.u(
            '{0}.md').format(fileInfo.baseName())
        self.updateLastFilesMenu(baseName=baseName)
        self.preview()

    def doSave(self, fileName):
        """
        enregistrement réel d'un fichier.
        Procédure appelée depuis saveFile ou saveFileAs.
        On insère l'entête perso au début du fichier.
        Elle permet de sauvegarder divers réglages (title, etc)
        """
        utils_functions.doWaitCursor()
        try:
            if len(self.titleEdit.text()) > 0:
                titleText = utils_functions.u(
                    'TITLE:{0}\n').format(self.titleEdit.text())
            else:
                titleText = ''
            if len(self.cssEdit.text()) > 0:
                cssText = utils_functions.u(
                    'CSS:{0}\n').format(self.cssEdit.text())
            else:
                cssText = ''
            if self.actionWithMathJax.isChecked():
                withMathJaxText = 'MATHJAX\n'
            else:
                withMathJaxText = ''
            lines = utils_functions.u(
                '<!--\n'
                'HEADER FOR PAMPI CONFIG\n'
                '{1}{2}{3}'
                '-->\n'
                '\n\n'
                '{0}')
            lines = utils_functions.u(lines).format(
                self.markdownEditor.toPlainText(), 
                titleText, 
                cssText, 
                withMathJaxText)
            QtCore.QFile(fileName).remove()
            outFile = QtCore.QFile(fileName)
            if outFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
                stream = QtCore.QTextStream(outFile)
                stream.setCodec('UTF-8')
                stream << lines
                outFile.close()
            self.updateWindowTitle()
        finally:
            utils_functions.restoreCursor()

    def saveFile(self):
        """
        enregistrement d'un fichier.
        """
        if len(self.actualFile['baseName']) < 1:
            self.saveFileAs()
            return
        fileName = utils_functions.u('{0}/md/{1}.md').format(
            self.presentationsDir, self.actualFile['baseName'])
        self.doSave(fileName)

    def saveFileAs(self):
        """
        enregistrement d'un fichier.
        """
        proposedName = self.actualFile['baseName']
        if len(proposedName) < 1:
            proposedName = QtWidgets.QApplication.translate(
                'main', 'No name')
        proposedName = utils_functions.u('{0}/md/{1}.md').format(
            self.presentationsDir, proposedName)
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self,
            QtWidgets.QApplication.translate(
                'main', 'Save as...'),
            proposedName,
            QtWidgets.QApplication.translate(
                'main', 'Markdown Files (*.md)'))
        if len(fileName) < 1:
            return
        if isinstance(fileName, tuple):
            fileName = fileName[0]
        # on met à jour actualFile :
        fileInfo = QtCore.QFileInfo(fileName)
        self.actualFile = {
            'baseName': fileInfo.baseName(), 
            'mustSave': False}
        # on met à jour les fichiers récents :
        baseName = utils_functions.u(
            '{0}.md').format(fileInfo.baseName())
        self.updateLastFilesMenu(baseName=baseName)
        # on enregistre le fichier :
        self.doSave(fileName)

    def export(self):
        """
        export d'un fichier dans un dossier.
        On sélectionne le dossier parent.
        On crée un sous-dossier portant le nom du fichier de présentation
        et on y recopie tout le nécessaire.
        Enfin on ouvre le dossier.
        """
        if not(self.testMustSave()):
            return

        # sélection du dossier de destination :
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self, 
            QtWidgets.QApplication.translate(
                'main', 'Select a directory to export the presentation'), 
            QtCore.QDir.homePath(), 
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if len(directory) < 1:
            return
        if isinstance(directory, tuple):
            directory = directory[0]
        #print('directory:', directory)

        # création des sous-dossiers :
        for dirName in ('assets', 'data', 'md'):
            utils_filesdirs.createDirs(
                directory, 
                utils_functions.u('{0}/{1}').format(
                    self.actualFile['baseName'], dirName))
        # copie des fichiers md et html :
        fileName = utils_functions.u('{0}/md/{1}.md').format(
            self.presentationsDir, self.actualFile['baseName'])
        utils_filesdirs.removeAndCopy(
            utils_functions.u('{0}/md/{1}.md').format(
                self.presentationsDir, self.actualFile['baseName']), 
            utils_functions.u('{0}/{1}/md/{1}.md').format(
                directory, self.actualFile['baseName']))
        utils_filesdirs.removeAndCopy(
            utils_functions.u('{0}/{1}.html').format(
                self.presentationsDir, self.actualFile['baseName']), 
            utils_functions.u('{0}/{1}/{1}.html').format(
                directory, self.actualFile['baseName']))
        # copie du dossier assets :
        utils_filesdirs.copyDir(
            utils_functions.u('{0}/assets').format(
                self.presentationsDir), 
            utils_functions.u('{0}/{1}/assets').format(
                directory, self.actualFile['baseName']))

        # récupération des fichiers utilisés et copie dans data :
        data = []
        for (before, after) in (('](data/', ')'), ('"data/', '"')):
            lines = self.markdownEditor.toPlainText().split(before)
            for line in lines[1:]:
                line2 = line.split(after)[0]
                if len(line2) > 0:
                    if not(line2 in  data):
                        data.append(line2)
        #print('data:', data)
        for fileName in data:
            newDirs = utils_functions.u('/').join(fileName.split('/')[:-1])
            if len(newDirs) > 0:
                utils_filesdirs.createDirs(
                    utils_functions.u('{0}/{1}/data').format(
                        directory, self.actualFile['baseName']), 
                    newDirs)
            utils_filesdirs.removeAndCopy(
                utils_functions.u('{0}/data/{1}').format(
                    self.presentationsDir, fileName), 
                utils_functions.u('{0}/{1}/data/{2}').format(
                    directory, self.actualFile['baseName'], fileName))

        # pour terminer on ouvre le dossier :
        utils_filesdirs.openDir(utils_functions.u('{0}/{1}').format(
            directory, self.actualFile['baseName']))

    def doCreateDesktopFileLinux(self):
        """
        Sous GNU/Linux, propose de créer un fichier .desktop
        pour lancer le logiciel.
        """
        title = QtWidgets.QApplication.translate(
            'main', 'Choose the Directory where the desktop file will be created')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self, 
            title, 
            QtCore.QDir.homePath(), 
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if directory != '':
            result = utils_filesdirs.createDesktopFile(
                self, directory, 'PAMPI', 'icon', 'pampi')

    def saveDoShow(self):
        """
        enchaînement des 3 actions
        """
        self.saveFile()
        self.doPandoc()
        self.preview()

    def searchPandoc(self):
        """
        cherche si Pandoc est bien installé
        """
        pandocBin = ''
        if utils.OS_NAME[0] == 'win':
            pandocBin = utils_functions.u(
                '{0}\libs\win\pandoc.exe').format(
                    QtCore.QDir.toNativeSeparators(self.beginDir))
        else:
            possiblePaths = ('/usr/local/bin', '/opt', '/usr/bin', )
            for possiblePath in possiblePaths:
                fileName = utils_functions.u('{0}/pandoc').format(possiblePath)
                if QtCore.QFileInfo(fileName).isFile():
                    pandocBin = fileName
                    break
        if len(pandocBin) < 1:
            link = utils.HELPPAGE
            m1 = QtWidgets.QApplication.translate(
                'main', 'PANDOC IS NOT INSTALLED.')
            m2 = QtWidgets.QApplication.translate(
                'main', 'This is the tool to convert Markdown files to html.')
            m3 = QtWidgets.QApplication.translate(
                'main', 'See the PAMPI help page:')
            message = utils_functions.u(
                '<p align="center"><b>{0}</b></p>'
                '<p align="center">__________________________</p>'
                '<p align="center">{1}</p>'
                '<p align="center">{2} <a href="{3}">PAMPI</a>.</p>'
                '<p></p>').format(m1, m2, m3, link)
            utils_functions.messageBox(self, message=message)
        return pandocBin

    def doPandoc(self, withMathjax=False):
        """
        mise en place et exécution du process de Pandoc
        """
        if len(self.actualFile['baseName']) < 1:
            return
        utils_functions.doWaitCursor()
        try:
            QtCore.QDir.setCurrent(self.presentationsDir)
            if len(self.titleEdit.text()) > 0:
                title = self.titleEdit.text()
            else:
                title = self.actualFile['baseName']
            if len(self.cssEdit.text()) > 0:
                css = self.cssEdit.text()
            else:
                css = 'default'
            withMathJax = self.actionWithMathJax.isChecked()

            if utils.OS_NAME[0] == 'win':
                args = [
                    '--template', 
                    utils_functions.u(
                        '{0}\\assets\_template.html').format(
                            QtCore.QDir.toNativeSeparators(self.presentationsDir))]
            else:
                args = ['--template', 'assets/_template.html']
            args.extend([
                '-V', utils_functions.u('css={0}').format(css), 
                '-V', utils_functions.u('title={0}').format(title)])
            if withMathJax:
                args.extend([
                    '-V', 'math=<script type="text/javascript" src="assets/js/MathJax/MathJax.js?config=TeX-MML-AM_CHTML"></script>', 
                    '-s', '--mathjax'])
            else:
                args.extend(['-V', 'math='])
            args.extend([
                '-s', '-t', 'html5', '--section-divs', 
                '-o'])
            if utils.OS_NAME[0] == 'win':
                args.append(
                    utils_functions.u('{0}\{1}.html').format(
                        QtCore.QDir.toNativeSeparators(self.presentationsDir), 
                        self.actualFile['baseName']))
                args.append(
                    utils_functions.u('{0}\md\{1}.md').format(
                        QtCore.QDir.toNativeSeparators(self.presentationsDir), 
                        self.actualFile['baseName']))
            else:
                args.append(utils_functions.u('{0}.html').format(self.actualFile['baseName']))
                args.append(utils_functions.u('md/{0}.md').format(self.actualFile['baseName']))

            #print(self.pandocBin, ' '.join(args))
            process = QtCore.QProcess(self)
            process.start(self.pandocBin, args)
            if not process.waitForStarted(3000):
                QtCore.qDebug('BUG IN PROCESS')
                return False
            if not process.waitForFinished():
                return False
        finally:
            QtCore.QDir.setCurrent(self.beginDir)
            utils_functions.restoreCursor()

    def preview(self):
        """
        (re)charge le fichier html de la présentation
        """
        if len(self.actualFile['baseName']) < 1:
            return
        utils_functions.doWaitCursor()
        try:
            outFileName = utils_functions.u(
                '{0}/{1}.html').format(
                    self.presentationsDir, self.actualFile['baseName'])
            if outFileName == self.helpWebView.url().path():
                self.helpWebView.load(self.helpWebView.url())
            else:
                url = QtCore.QUrl().fromLocalFile(outFileName)
                self.helpWebView.load(url)
        finally:
            utils_functions.restoreCursor()

    def linkClicked(self, url):
        QtGui.QDesktopServices.openUrl(url)

    def launchInBrowser(self):
        if len(self.actualFile['baseName']) < 1:
            return
        utils_filesdirs.openFile(
            utils_functions.u('{0}/{1}.html').format(
                self.presentationsDir, self.actualFile['baseName']))

    def openPresentationDir(self):
        utils_filesdirs.openDir(self.presentationsDir)

    def changePresentationDir(self):#ICI
        """
        modification du dossier des présentations.
        On sélectionne le nouveau dossier.
        Tout est ensuite recopié
        """
        # sélection du nouveau dossier :
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self, 
            QtWidgets.QApplication.translate(
                'main', 'Select a directory'), 
            QtCore.QDir.homePath(), 
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if len(directory) < 1:
            return
        if isinstance(directory, tuple):
            directory = directory[0]
        utils_functions.doWaitCursor()
        try:
            # copie du dossier :
            utils_filesdirs.copyDir(
                self.presentationsDir, 
                directory)
            self.presentationsDir = directory
            self.configDict['OTHER']['presentationsDir'] = directory
        finally:
            QtCore.QDir.setCurrent(self.beginDir)
            utils_functions.restoreCursor()

    def showHelp(self, here=False):
        """
        permet à la fois de charger la présentation d'aide au lancement
        et d'ouvrir l'aide dans le navigateur
        """
        if here:
            url = QtCore.QUrl().fromLocalFile(
                utils_functions.u(
                    '{0}/pampi-help.html').format(self.presentationsDir))
            self.helpWebView.load(url)
        else:
            url = QtCore.QUrl(utils.HELPPAGE)
            QtGui.QDesktopServices.openUrl(url)

    def about(self):
        """
        affichage de la fenêtre À propos
        """
        import utils_about
        aboutdialog = utils_about.AboutDlg(
            self, self.locale, icon='./images/icon.svgz')
        lastState = self.disableInterface(aboutdialog)
        aboutdialog.exec_()
        self.enableInterface(aboutdialog, lastState)

    def showExamples(self):
        """
        affichage du pense-bête
        """
        checked = self.actionShowExamples.isChecked()
        self.editExamplesCheckBox.setVisible(checked)
        if checked:
            self.whatStackedWidget.setCurrentIndex(1)
        else:
            self.whatStackedWidget.setCurrentIndex(0)

    def editExamples(self):
        """
        pour rendre le fichier pense-bête modifiable
        """
        checked = self.editExamplesCheckBox.isChecked()
        if checked:
            self.samplesEditor.setReadOnly(False)
        else:
            # on enregistre le fichier d'exemple :
            lines = self.samplesEditor.toPlainText()
            QtCore.QFile(self.examplesFile).remove()
            outFile = QtCore.QFile(self.examplesFile)
            if outFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
                stream = QtCore.QTextStream(outFile)
                stream.setCodec('UTF-8')
                stream << lines
                outFile.close()
            self.samplesEditor.setReadOnly(True)

    def wizard(self):
        """
        aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
        """
        print('wizard')
        # faut-il enregistrer avant ?
        
        # dialog de l'assistant :
        import utils_wizard
        dialog = utils_wizard.WizardDlg(
            self, self.locale, icon='./images/icon.svgz')
        lastState = self.disableInterface(dialog)

        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            # 
            print('OK !!! index:', dialog.index)
            #print(dialog.lines)

            self.markdownEditor.setPlainText(dialog.lines)

        self.enableInterface(dialog, lastState)





