# -*- coding: utf-8 -*-

# -----------------------------------------------------------------
# This file is a part of PAMPI project.
# Name:         PAMPI
# Copyright:    (C) 2017 Pascal PETER
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
# -----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------

"""
DESCRIPTION :
    blablabla
"""

# importation des modules utiles :
from __future__ import division, print_function
import sys
import os



"""
****************************************************
    VERSIONS DE PYTHON, QT, ETC
****************************************************
"""

# version de Python :
PYTHONVERSION = sys.version_info[0] * 10 + sys.version_info[1]

# PyQt5, PyQt4 ou PySide :
PYQT = ''
if 'PYSIDE' in sys.argv:
    try:
        from PySide import QtCore, QtGui as QtWidgets, QtGui
        PYQT = 'PYSIDE'
    except:
        pass
elif 'PYQT4' in sys.argv:
    try:
        import sip
        sip.setapi('QString', 2)
        sip.setapi('QVariant', 2)
        from PyQt4 import QtCore, QtGui as QtWidgets, QtGui
        PYQT = 'PYQT4'
    except:
        pass
else:
    try:
        # on teste d'abord PyQt5 :
        from PyQt5 import QtCore, QtWidgets, QtGui
        PYQT = 'PYQT5'
    except:
        # puis PyQt4 :
        try:
            import sip
            sip.setapi('QString', 2)
            sip.setapi('QVariant', 2)
            from PyQt4 import QtCore, QtGui as QtWidgets, QtGui
            PYQT = 'PYQT4'
        except:
            # puis enfin PySide :
            try:
                from PySide import QtCore, QtGui as QtWidgets, QtGui
                PYQT = 'PYSIDE'
            except:
                pass
if PYQT == '':
    print('YOU MUST INSTALL PYQT !')
else:
    print('PYTHONVERSION:', PYTHONVERSION, 'PYQT:', PYQT)

# version de Qt :
qtVersion = QtCore.qVersion()

# détection du système (nom et 32 ou 64) :
OS_NAME = ['', '']
def detectPlatform():
    global OS_NAME
    # 32 ou 64 bits :
    if sys.maxsize > 2**32:
        bits = 64
    else:
        bits = 32
    # platform et osName :
    platform = sys.platform
    osName = ''
    if platform.startswith('linux'):
        osName = 'linux'
    elif platform.startswith('win'):
        osName = 'win'
    elif platform.startswith('freebsd'):
        osName = 'freebsd'
    elif platform.startswith('darwin'):
        import platform
        if 'powerpc' in platform.uname():
            osName = 'powerpc'
        else:
            osName = 'mac'
    OS_NAME = [osName, bits]
detectPlatform()

MODEBAVARD = False
if 'MODEBAVARD' in sys.argv:
    MODEBAVARD = True



"""
****************************************************
    VARIABLES LIÉES AU LOGICIEL
****************************************************
"""

PROGTITLE = 'PAMPI'
PROGNAME = 'pampi'
PROGVERSION = '1.0'
HELPPAGE = 'http://pascal.peter.free.fr/pampi.html'



"""
****************************************************
    DIVERS
****************************************************
"""

DEFAULTCONFIG = {
    'LASTFILES': [], 
    'OTHER': {
        'presentationsDir': '', 
        },
    }



STYLE = {}
def loadStyle():
    global STYLE
    style = QtWidgets.QApplication.style()
    STYLE = {
        #'PM_ToolBarIconSize': style.pixelMetric(QtWidgets.QStyle.PM_ToolBarIconSize), 
        'PM_LargeIconSize': style.pixelMetric(QtWidgets.QStyle.PM_LargeIconSize), 
        #'PM_SmallIconSize': style.pixelMetric(QtWidgets.QStyle.PM_SmallIconSize), 
        #'PM_IconViewIconSize': style.pixelMetric(QtWidgets.QStyle.PM_IconViewIconSize), 
        #'PM_ListViewIconSize': style.pixelMetric(QtWidgets.QStyle.PM_ListViewIconSize), 
        #'PM_TabBarIconSize': style.pixelMetric(QtWidgets.QStyle.PM_TabBarIconSize), 
        #'PM_MessageBoxIconSize': style.pixelMetric(QtWidgets.QStyle.PM_MessageBoxIconSize), 
        #'PM_ButtonIconSize': style.pixelMetric(QtWidgets.QStyle.PM_ButtonIconSize), 
        }


SUPPORTED_IMAGE_FORMATS = ('png',)

def loadSupportedImageFormats():
    global SUPPORTED_IMAGE_FORMATS
    SUPPORTED_IMAGE_FORMATS = QtGui.QImageReader.supportedImageFormats()

def doIcon(fileName='', ext='svgz'):
    if ext in SUPPORTED_IMAGE_FORMATS:
        return QtGui.QIcon('images/{0}.{1}'.format(fileName, ext))
    else:
        return QtGui.QIcon('images/png/{0}.png'.format(fileName))

def doPixmap(fileName='', ext='svgz'):
    if ext in SUPPORTED_IMAGE_FORMATS:
        return QtGui.QPixmap('images/{0}.{1}'.format(fileName, ext))
    else:
        return QtGui.QPixmap('images/png/{0}.png'.format(fileName))

