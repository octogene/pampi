
## YOU CAN COPY-PASTE AND ADAPT THE EXAMPLES BELOW
## IN YOUR PRESENTATION FILES

*********************************************
*********************************************
## EXAMPLES OF STEPS
*********************************************
*********************************************

# {.step data-x=1000 data-y=1000 data-rotate=60 data-scale=2}

# {.step data-x=1000 data-y=1000}

# {.step .text-left data-x=1000 data-y=1000}


### List of directives

* **coordinates** : data-x, data-y, data-z
* **zoom** : data-scale
* **rotations** : data-rotate, data-rotate-x, data-rotate-y
* **alignment** : .text-left, .text-right







*********************************************
*********************************************
## MARKDOWN FORMAT EXAMPLES
*********************************************
*********************************************

## Level 2 Title
### Level 3 Title
#### Level 4 Title
##### Level 5 Title
###### Level 6 Title

----

Normal text

In **bold**, in *italic* or ~~barred~~



### Lists

* a bulleted list
* the following
     * a lower inlet
     * after
* return to the first level


1. a numbered list
#. the following
     I. a lower inlet
     #. after
#. we return to the first level


### Links

[Wikipédia](https://fr.wikipedia.org)

[a PDF file](data/pampi-help/tableau_conversion_volumes.pdf)

[a GeoGebra file](data/pampi-help/n4a.ggb)


### A picture

![](data/pampi-help/splash.png)



### Many examples are available on the page below

http://enacit1.epfl.ch/markdown-pandoc

### To write mathematical formulas

https://en.wikibooks.org/wiki/LaTeX/Mathematics







*********************************************
*********************************************
## SOME CHARACTERS WHICH MAY BE USEFUL
*********************************************
*********************************************

### a non-displayable character to make a blank line




### mathematical characters

√ + - × ÷ ± ≠ ↦ ⟼ ∈ ∉ ≈ → ≤ ⊥ π ∞ 

½ « » ’ … ‰ € – — ‐ ⸮ ¹ ² ³ ⁴ ⁵ ⁶ ⁷ ⁸ ⁹ ⁰






*********************************************
*********************************************

## TO WRITE NOTES
## VISIBLE IN THE CONSOLE
*********************************************
*********************************************

<div class="notes">
Text of the note (with any formatting)
</div>



