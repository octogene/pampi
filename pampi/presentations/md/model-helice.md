<!--
HEADER FOR PAMPI CONFIG
TITLE:titre
-->


# {.step data-x=0 data-y=-1000 data-z=-1000 data-rotate-y=0}

## 0

![](data/pampi-help/splash.png)



# {.step data-x=866 data-y=-500 data-z=-500 data-rotate-y=-60}

## 1

![](data/pampi-help/splash.png)



# {.step data-x=866 data-y=0 data-z=500 data-rotate-y=-120}

## 2

![](data/pampi-help/splash.png)



# {.step data-x=0 data-y=500 data-z=1000 data-rotate-y=-180}

## 3

![](data/pampi-help/splash.png)



# {.step data-x=-866 data-y=1000 data-z=500 data-rotate-y=-240}

## 4

![](data/pampi-help/splash.png)



# {.step data-x=-866 data-y=1500 data-z=-500 data-rotate-y=-300}

## 5

![](data/pampi-help/splash.png)













<!--
import math

n = 6
r = 1000
pas = 500
for i in range(n):
    a = -i * 2 * math.pi / n
    d = round(180 * a / math.pi)
    x = - round(r * math.sin(a))
    y = i * pas
    z = - round(r * math.cos(a))
    #print(i + 1, d, x, y)
    print('# {4}.step data-x={0} data-y={1} data-z={2} data-rotate-y={3}{5}\n'.format(x, y, z, d, '{', '}'))
    print('## {0}\n'.format(i))
    print('![](data/pampi-help/splash.png)\n\n\n')
-->




# {#overview .step data-scale=4}