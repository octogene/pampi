<!--
HEADER FOR PAMPI CONFIG
TITLE:D'après la démo d'impress.js
CSS:impress-demo
-->



<!--
Here is where interesting thing start to happen.
Each step of the presentation should be an element inside the `#impress` with a class name of `step`. These step elements are positioned,  rotated and scaled by impress.js, and the 'camera' shows them on each step of the presentation.
Positioning information is passed through data attributes.
In the example below we only specify x and y position of the step element with `data-x="-1000"` and `data-y="-1500"` attributes. This means that **the center** of the element (yes, the center) will be positioned in point x = -1000px and y = -1500px of the presentation 'canvas'.
It will not be rotated or scaled.
-->

# {.step .slide data-x=-4500 data-y=-3000}

#### Vous n'en avez pas



## **un peu marre**



#### de toutes ces présentations à base de diapositives ?







# {.step .slide data-x=-3000 data-y=-3000}





### Qui défilent les unes après les autres.








# {.step .slide data-x=-1500 data-y=-3000}



## Vous voudriez impressionner votre public ?









#  {.step data-x=-3000 data-y=-1500 data-scale=6}

##### Alors vous devriez essayer

##  ![](data/pampi-help/pampi_icon.png) PAMPI










#  {.step .text-left data-x=-3150 data-y=2000 data-rotate=90 data-scale=5}

C'est un **outil de présentation** \
basé sur l'utilisation de 
[Markdown](http://daringfireball.net/projects/markdown), 
[Pandoc](http://www.pandoc.org) 
et [impress.js](https://github.com/impress/impress.js).

Il utilise la puissance des **transformations CSS3**  \
et des **transitions** dans les navigateurs modernes.








#  {.step data-x=500 data-y=1100 data-rotate=180 data-scale=14}

Montrez vos

## **grandes**

#### idées











#  {.step data-x=-175 data-y=1325 data-z=-3000 data-rotate=300 data-scale=1}

#### comme vos <small>toutes petites</small> idées











#  {#ing .step data-x=500 data-y=-3000 data-rotate=270 data-scale=6}

en les <b class="positioning">positionnant</b>, \
en les <b class="rotating">tournant</b> et \
en les <b class="scaling">redimensionnant</b> \
dans un espace infini








#  {.step data-x=3700 data-y=-1300 data-scale=6}

la seule **limite** est votre **imagination**






#  {.step data-x=4000 data-y=1000 data-rotate=20 data-scale=4}

Vous voulez en savoir plus ?

[C'est ici](http://pascal.peter.free.fr/pampi.html) !






#  {.step data-x=3000 data-y=3000 data-scale=2}

encore une chose...











#  {.step data-x=3200 data-y=3300 data-z=-100 data-rotate-x=-40 data-rotate-y=10 data-scale=2}

Avez-vous remarqué que c'est en **3D** ?

















# {#overview .step data-scale=10}
