<!--
HEADER FOR PAMPI CONFIG
TITLE:Console de présentation
-->



# {.step data-y=-2000}

#### LA CONSOLE DE PRÉSENTATION

* la console de présentation vous permet d'avoir une fenêtre séparée contenant
    * un aperçu de la diapositive actuelle
    * un aperçu de la diapositive suivante
    * vos notes
    * des boutons pour gérer le changement de diapositive
    * une horloge et une minuterie réinitialisable (par clic)
* elle utilise [impress-console](https://github.com/regebro/impress-console)

<div class="notes">
Bienvenue dans la console de présentation.
</div>







# {.step data-y=-1000}

## IMPORTANT

* la console de présentation ne s'affichera pas dans PAMPI mais seulement dans votre navigateur
* elle est activée par la touche **P** du clavier

<div class="notes">
#### Vos notes peuvent être formatées

* des listes
* des [liens](https://github.com/regebro/impress-console)
* des images
* etc

![](data/pampi-help/splash.png)
</div>






# {.step data-y=0}

#### VOICI À QUOI ELLE RESSEMBLE

![](data/pampi-help/impress-console.jpeg)







# {.step data-y=1000}

## POUR ÉCRIRE DES NOTES

Il suffit de les mettre entre les 2 lignes suivantes

* `<div class="notes">`
* `</div>`







# {.step data-y=2000}

## PAR EXEMPLE

la note qui accompagne cette étape est

                <div class="notes">  
    texte de la note
    </div>

<div class="notes">
texte de la note
</div>








# {#overview .step data-scale=6}
