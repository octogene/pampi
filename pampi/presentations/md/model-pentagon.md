<!--
HEADER FOR PAMPI CONFIG
TITLE:titre
-->




#  {.step data-scale=4}

## 5 QUESTIONS













#  {.step data-x=4000 data-y=0 data-scale=2 data-rotate=-90}

## Question 1

![](data/pampi-help/splash.png)




#  {.step data-x=1236 data-y=-3804 data-scale=2 data-rotate=-162}

## Question 2

![](data/pampi-help/splash.png)




#  {.step data-x=-3236 data-y=-2351 data-scale=2 data-rotate=-234}

## Question 3

![](data/pampi-help/splash.png)




#  {.step data-x=-3236 data-y=2351 data-scale=2 data-rotate=-306}

## Question 4

![](data/pampi-help/splash.png)




#  {.step data-x=1236 data-y=3804 data-scale=2 data-rotate=-378}

## Question 5

![](data/pampi-help/splash.png)













<!--
import math

n = 5
r = 4000
for i in range(n):
    a = -i * 2 * math.pi / n
    d = round(-90 + 180 * a / math.pi)
    x = round(r * math.cos(a))
    y = round(r * math.sin(a))
    #print(i + 1, d, x, y)
    print('#  {3}.step data-x={0} data-y={1} data-rotate={2}{4}\n'.format(x, y, d, '{', '}'))
    print('## Question {0}\n'.format(i + 1))
    print('![](data/pampi-help/splash.png)\n\n\n')
-->





# {#overview .step data-scale=12}