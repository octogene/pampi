SOURCES      += ../pampi.pyw

SOURCES      += ../libs/main.py
SOURCES      += ../libs/utils.py
SOURCES      += ../libs/utils_about.py
SOURCES      += ../libs/utils_filesdirs.py
SOURCES      += ../libs/utils_functions.py
SOURCES      += ../libs/utils_wizard.py

TRANSLATIONS += ../translations/pampi.ts
TRANSLATIONS += ../translations/pampi_fr.ts
