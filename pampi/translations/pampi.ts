<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context>
    <name>main</name>
    <message>
        <location filename="../libs/main.py" line="277"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="339"/>
        <source>Create a launcher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="263"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="379"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="409"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="490"/>
        <source>The file has been modified.
Do you want to save your changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="822"/>
        <source>Choose the Directory where the desktop file will be created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="68"/>
        <source>About {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="72"/>
        <source>(version {0})</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="91"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="94"/>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="99"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="167"/>
        <source>information message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="168"/>
        <source>question message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="169"/>
        <source>warning message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="170"/>
        <source>critical message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="210"/>
        <source>END !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="211"/>
        <source>Images are saved in the folder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="270"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="284"/>
        <source>Ctrl+Shift+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="297"/>
        <source>Save, convert and preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="303"/>
        <source>Convert to html with Pandoc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="309"/>
        <source>Preview presentation in PAMPI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="315"/>
        <source>Open in the browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="382"/>
        <source>Recent Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="397"/>
        <source>Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="609"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="717"/>
        <source>Markdown Files (*.md)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="713"/>
        <source>No name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="717"/>
        <source>Save as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="263"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="277"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="327"/>
        <source>Open the presentation folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="321"/>
        <source>Use MathJax</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="291"/>
        <source>Export the presentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="752"/>
        <source>Select a directory to export the presentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="860"/>
        <source>PANDOC IS NOT INSTALLED.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="862"/>
        <source>This is the tool to convert Markdown files to html.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="864"/>
        <source>See the PAMPI help page:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="90"/>
        <source>Select a directory for presentations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="151"/>
        <source>Title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="333"/>
        <source>Change the presentation folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="978"/>
        <source>Select a directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="163"/>
        <source>CSS:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="156"/>
        <source>Here you can define the title that will be displayed in the browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="168"/>
        <source>If you are using a custom CSS file, enter its name here (without extension)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="357"/>
        <source>Cheat Sheet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="363"/>
        <source>A list of examples to copy-paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="199"/>
        <source>Edit Cheat Sheet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="203"/>
        <source>Make the file editable to add your own examples</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="369"/>
        <source>Wizard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="70"/>
        <source>File Creation Wizard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="92"/>
        <source>regular polygon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="93"/>
        <source>arrangement in table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="94"/>
        <source>circular helix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="99"/>
        <source>aaaaaaaaaaaaaa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="101"/>
        <source>zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="117"/>
        <source>Select a presentation template:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="129"/>
        <source>Number of steps (not counting the title):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="139"/>
        <source>Radius:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="149"/>
        <source>Offset:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="159"/>
        <source>Number of columns:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="169"/>
        <source>Number of rows:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="190"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="194"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="95"/>
        <source>carousel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
