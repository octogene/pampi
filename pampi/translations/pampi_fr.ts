<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>main</name>
    <message>
        <location filename="../libs/main.py" line="263"/>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="263"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="409"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="91"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="379"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="94"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="99"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="68"/>
        <source>About {0}</source>
        <translation>À propos de {0}</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="167"/>
        <source>information message</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="168"/>
        <source>question message</source>
        <translation>Question</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="169"/>
        <source>warning message</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="170"/>
        <source>critical message</source>
        <translation>Message critique</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="822"/>
        <source>Choose the Directory where the desktop file will be created</source>
        <translation>Choisissez le dossier où le lanceur sera créé</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="339"/>
        <source>Create a launcher</source>
        <translation>Créer un lanceur</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="490"/>
        <source>The file has been modified.
Do you want to save your changes?</source>
        <translation>Le fichier a été modifié.
Voulez-vous enregistrer les changements ?</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="277"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="277"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="210"/>
        <source>END !</source>
        <translation>TERMINÉ !</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="211"/>
        <source>Images are saved in the folder:</source>
        <translation>Les images sont enregistrées dans le dossier :</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="72"/>
        <source>(version {0})</source>
        <translation>(version {0})</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="609"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="270"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="717"/>
        <source>Save as...</source>
        <translation>Enregistrer sous...</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="284"/>
        <source>Ctrl+Shift+S</source>
        <translation>Ctrl+Shift+S</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="297"/>
        <source>Save, convert and preview</source>
        <translation>Enregistrer, convertir et visualiser</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="303"/>
        <source>Convert to html with Pandoc</source>
        <translation>Convertir en html avec Pandoc</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="309"/>
        <source>Preview presentation in PAMPI</source>
        <translation>Visualiser la présentation dans PAMPI</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="315"/>
        <source>Open in the browser</source>
        <translation>Ouvrir dans le navigateur</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="382"/>
        <source>Recent Files</source>
        <translation>Fichiers récents</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="397"/>
        <source>Tools</source>
        <translation>Outils</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="717"/>
        <source>Markdown Files (*.md)</source>
        <translation>Fichiers Markdown (*.md)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="713"/>
        <source>No name</source>
        <translation>Sans nom</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="327"/>
        <source>Open the presentation folder</source>
        <translation>Ouvrir le dossier des présentations</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="321"/>
        <source>Use MathJax</source>
        <translation>Utiliser MathJax</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="291"/>
        <source>Export the presentation</source>
        <translation>Exporter la présentation</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="752"/>
        <source>Select a directory to export the presentation</source>
        <translation>Sélectionnez un dossier pour exporter la présentation</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="860"/>
        <source>PANDOC IS NOT INSTALLED.</source>
        <translation>PANDOC N&apos;EST PAS INSTALLÉ.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="862"/>
        <source>This is the tool to convert Markdown files to html.</source>
        <translation>C&apos;est l&apos;outil qui permet de convertir les fichiers Markdown en html.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="864"/>
        <source>See the PAMPI help page:</source>
        <translation>Voir la page d&apos;aide de PAMPI :</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="90"/>
        <source>Select a directory for presentations</source>
        <translation>Sélectionnez un dossier pour vos présentations</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="151"/>
        <source>Title:</source>
        <translation>Titre :</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="333"/>
        <source>Change the presentation folder</source>
        <translation>Changer le dossier des présentations</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="978"/>
        <source>Select a directory</source>
        <translation>Sélectionnez un dossier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="163"/>
        <source>CSS:</source>
        <translation>CSS :</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="156"/>
        <source>Here you can define the title that will be displayed in the browser</source>
        <translation>Vous pouvez définir ici le titre qui sera affiché dans le navigateur</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="168"/>
        <source>If you are using a custom CSS file, enter its name here (without extension)</source>
        <translation>Si vous utilisez un fichier CSS personnalisé, indiquez ici son nom (sans extension)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="357"/>
        <source>Cheat Sheet</source>
        <translation>Pense-bête</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="363"/>
        <source>A list of examples to copy-paste</source>
        <translation>Une liste d&apos;exemples à copier-coller</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="199"/>
        <source>Edit Cheat Sheet</source>
        <translation>Modifier le pense-bête</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="203"/>
        <source>Make the file editable to add your own examples</source>
        <translation>Rendre le fichier éditable pour ajouter vos propres exemples</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="369"/>
        <source>Wizard</source>
        <translation>Assistant</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="70"/>
        <source>File Creation Wizard</source>
        <translation>Assistant de création de fichier</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="92"/>
        <source>regular polygon</source>
        <translation>polygone régulier</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="93"/>
        <source>arrangement in table</source>
        <translation>disposition en tableau</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="94"/>
        <source>circular helix</source>
        <translation>hélice circulaire</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="99"/>
        <source>aaaaaaaaaaaaaa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="101"/>
        <source>zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="117"/>
        <source>Select a presentation template:</source>
        <translation>Sélectionnez un modèle de présentation :</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="129"/>
        <source>Number of steps (not counting the title):</source>
        <translation>Nombre d&apos;étapes (sans compter le titre) :</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="139"/>
        <source>Radius:</source>
        <translation>Rayon :</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="149"/>
        <source>Offset:</source>
        <translation>Décalage :</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="159"/>
        <source>Number of columns:</source>
        <translation>Nombre de colonnes :</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="169"/>
        <source>Number of rows:</source>
        <translation>Nombre de rangées :</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="190"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="194"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="95"/>
        <source>carousel</source>
        <translation>carrousel</translation>
    </message>
</context>
</TS>
