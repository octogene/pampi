# PAMPI
## <small>Présentations Avec Mardown, Pandoc, Impress. - Presentations With Markdown, Pandoc, Impress.</small>

----

* **Website:** http://pascal.peter.free.fr/pampi.html
* **Email:** pascal.peter at free.fr
* **License:** GNU General Public License (version 3)
* **Copyright:** (c) 2017

----

### Les outils utilisés pour développer PAMPI - The tools used to develop PAMPI

* [Python](http://www.python.org)
* [PyQt](http://www.riverbankcomputing.co.uk/software/pyqt/intro)
* [marked](https://github.com/chjj/marked)
* [MarkdownHighlighter](https://github.com/rupeshk/MarkdownHighlighter)

### et ceux utilisés pour les présentations - and those used for presentations

* [Markdown](http://daringfireball.net/projects/markdown)
* [Pandoc](http://www.pandoc.org)
* [impress.js](https://github.com/impress/impress.js)
* [MathJax](https://www.mathjax.org)
* [Hovercraft!](https://github.com/regebro/hovercraft)
* [impress-console](https://github.com/regebro/impress-console)
* [Bootstrap](http://getbootstrap.com)

